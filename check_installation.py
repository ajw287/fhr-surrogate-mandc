#!/python
# Copyright 2018 Andrew Whyte & Zhiyao Xing
#
#Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
#The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#

# based on detecting anaconda installations found at:
# https://stackoverflow.com/questions/21282363/any-way-to-tell-if-users-python-environment-is-anaconda
#
# anaconda
# pandas
# tensorflow
# keras

import sys
if 'Anaconda' in sys.version or 'Continuum' in sys.version:
	print ("Anaconda installation ... OK")
else:
	print("It appears that your python distribution is not Anaconda\n This shouldn't be a problem, but the dependencies must be installed individually: \n\tmatplotlib \n \tnumpy \n\tpickle\n\ttk-inter\n\tscipy\n\tkeras\n\th5py\n\ttensorflow (or other backend)")
try:
    import keras as k
except ImportError:
	#print("\n\ntry the command: >conda install -c conda-forge keras\n\n")
	raise ImportError("keras doesn't appear to be installed... \n\ntry the command: >conda install -c conda-forge keras\n\n")
else:
	print("keras installed, version: " + str(k.__version__))
try:
    import tensorflow as tf
except ImportError:
    #print("\n\ntry the command: > conda install -c conda-forge tensorflow\n\n")
    raise ImportError("tensorflow doesn't appear to be installed... \n\ntry the command: > conda install -c conda-forge tensorflow\n\n")
else:
	print("tensorflow installed,  version: "+str(tf.__version__))
