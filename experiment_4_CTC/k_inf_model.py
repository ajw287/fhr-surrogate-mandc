# Copyright 2018 Andrew Whyte & Zhiyao Xing
#
#Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
#The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#
import pandas as pd
import sys, os
import pickle
import numpy as np
import time

# have to set seeds before we import the keras
import random as rn
import tensorflow as tf

# make keras results repeatable, see:
# https://keras.io/getting-started/faq/#how-can-i-obtain-reproducible-results-using-keras-during-development
seed  = sys.argv[3]
# Max for PYTHONHASHSEED 4294967295
# Max for numpy 2**32
if seed == 'time':
    # shuffled time initialisation source:
    # https://github.com/schmouk/PyRandLib
    #t = int( time.time() * 1000.0 )
    #seed = str( ( ((t & 0x0f000000) >> 24) +
    #            ((t & 0x00ff0000) >>  8) +
    #            ((t & 0x0000ff00) <<  8) +
    #            ((t & 0x000000ff) << 24)   )  )
    seed = str(int( (time.time()*1000.0) % 2147483647))
#os.environ['PYTHONHASHSEED'] = '0'
np.random.seed(int(seed))
#rn.seed(np.random.randint(0, 2**32 - 1, dtype='l'))
rn.seed(np.random.randint(0, 2147483647, dtype='l'))
#
from keras import backend as K
tf.set_random_seed(np.random.randint(0, 2147483647, dtype='l'))
session_conf = tf.ConfigProto(intra_op_parallelism_threads=1, inter_op_parallelism_threads=1)
sess = tf.Session(graph=tf.get_default_graph(), config=session_conf)
K.set_session(sess)

from keras.models import Sequential
from keras.layers import Dense, Activation
#from keras.models import load_model
#from keras.models import model_from_json
from keras.optimizers import Adam
from sklearn.preprocessing import MinMaxScaler
from sklearn.model_selection import train_test_split

#https://machinelearningmastery.com/reproducible-results-neural-networks-keras/
# set both the numpy random seed and the tensorflow random seed...


class record:
    pass

#mlp_layers = 9
#neurons_per_layer = 25

#mlp_layers = int(sys.argv[4])
#neurons_per_layer = int(sys.argv[5])

mlp_layers = 3
neurons_per_layer = 90

description = "\nA script to investigate \& model excel tabulated AGR/FHR design results\n\n"
example_usage = "usage : \n" + str(sys.argv[0])+ " excel_document sheet_label random_seed\n\n eg:\n"+str(sys.argv[0])+" ALL_Kinf_results.xlsx annularPin_UC 0\n\n"

def usage_error(string = ""):
    print ("\n\nUSAGE ERROR: \n"+string)
    print (str(sys.argv[0]) + description + example_usage)
    quit()

def preprocess (ins, outs):
    # do I need two instances of this?  I would like to recover the data afterwards?
    scaler_outputs = MinMaxScaler(feature_range=(0, 1))
    scaler_inputs = MinMaxScaler(feature_range=(0, 1))
    outs = scaler_outputs.fit_transform(outs)
    print("\n\ndebug test:")
    print (len(ins) )
    ins = scaler_inputs.fit_transform(ins)
    return np.array(ins), np.array(outs)

def singlePin_UC(data_table):
    #drop the coolant column of data as it has been 1-k binarised in excel
    #data_table.drop('fuel', axis=1)
    #data_table.drop('coolant', axis=1)
    inputs = data_table.iloc[:, 3:9].values
    outputs = data_table.iloc[:, 9:10].values
    num_inputs= len(inputs[0])
    num_outputs = 1
    return inputs, outputs, num_inputs, num_outputs

def annularPin_UC(data_table):
    #drop the coolant column of data as it has been 1-k binarised in excel
    #data_table.drop('fuel', axis=1)
    #data_table.drop('coolant', axis=1)
    inputs = data_table.iloc[:, 3:9].values
    outputs = data_table.iloc[:, 9:10].values
    num_inputs = len(inputs[0]) # 6
    num_outputs = 1
    return inputs, outputs, num_inputs, num_outputs

def singlePin_Triso(data_table):
    #drop the coolant column of data as it has been 1-k binarised in excel
    #data_table.drop('fuel', axis=1)
    #data_table.drop('coolant', axis=1)
    inputs = data_table.iloc[:, 3:9].values
    outputs = data_table.iloc[:, 9:10].values
    num_inputs= len(inputs[0])
    num_outputs = 1
    return inputs, outputs, num_inputs, num_outputs

def annularPin_Triso(data_table):
    #drop the coolant column of data as it has been 1-k binarised in excel
    #data_table.drop('fuel', axis=1)
    #data_table.drop('coolant', axis=1)
    inputs = data_table.iloc[:, 3:9].values
    outputs = data_table.iloc[:, 9:10].values
    num_inputs= len(inputs[0])
    num_outputs = 1
    return inputs, outputs, num_inputs, num_outputs

def sol_OG(data_table):
    #data_table.drop(data_table.index[:19])
    # don't drop them, the iloc statement only chooses i/o
    #data_table.drop('fuel', axis=1)
    #data_table.drop('cool', axis=1)
    inputs = data_table.iloc[19:, [3,4,7,8,9,10,11,12]].values
    outputs = data_table.iloc[19:, 13:34].values
    num_inputs= len(inputs[0])
    num_outputs = len(outputs[0])
    return inputs, outputs, num_inputs, num_outputs

def ann_OG(data_table):
    inputs = data_table.iloc[19:, [3,4,7,8,9,10,11,12,13]].values
    outputs = data_table.iloc[19:, 14:34].values
    num_inputs= len(inputs[0])
    num_outputs = len(outputs[0])
    return inputs, outputs, num_inputs, num_outputs

def no_func(data_table):
    print("ERROR: function not implemented yet!")

def main():
    if 4 != len(sys.argv) or sys.argv[1] == "--help":
        usage_error()
    # set the random seed
    # uncomment the correct option
    #sheet_label = "annUC"
    #sheet_label = "solidPinUC_optimisationUse"
    #sheet_label = "solidPinTriso"
    #sheet_list = ["annularPin_UC", "singlePin_Triso"]

    # import the specified sheet of the data
    excel_file = sys.argv[1]
    sheet_label = sys.argv[2]
    xlsx_file = pd.ExcelFile(excel_file)
    sheet_list = xlsx_file.sheet_names
    print("sheets are: ")
    print(sheet_list)

    print ("Processing: "+str(sheet_label))
#    if sheet_label not in sheet_list:
#    	print ("Did not work - give a valid sheet name.  Options are: " + str(sheet_list))
#	exit()
    core_ave_DC_tot = []
    core_ave_CTC_tot = []
    bu_over_e = []
    for sheet_label in sheet_list:
        #try:
        #    os.path.exists(excel_file)
        #except:
        #    print("\n\nExcel file not found: " + excel_file + "\n")
        #    exit()
        fhr_data = pd.read_excel(excel_file, sheet_name=sheet_label)#, skiprows=17)
        print(fhr_data.head)
        print(fhr_data.shape)
        sheet_dict = {
            # old ones (pre 15/04/2019):
            'singlePin_UC': singlePin_UC,
            'annularPin_UC': annularPin_UC,
            'singlePin_Triso': singlePin_Triso,
            'annularPin_Triso': annularPin_Triso,
            # new ones (post 15/04/2019)
            'ALLsol_OG': sol_OG,
            'ALLann_OG': ann_OG,
            'solUC_FLiBe_OG': sol_OG,
            'solFCM_FLiBe_OG': sol_OG,
            'annFCM_FLiBe_OG': ann_OG,
            'solUC_NaF-ZrF4_OG': sol_OG,
#            'solUC_FLiBe': sol_OG,
#            'solFCM_FLiBe': sol_OG,
#            'annFCM_FLiBe': ann_OG,
#            'solUC_NaF-ZrF4': sol_OG
            }

        # python switch statements are horrible.
        try:
            inputs, outputs, num_inputs, num_outputs = sheet_dict[sheet_label](fhr_data)

            #scales...
            X, Y = preprocess(inputs, outputs)

            print(X.shape)
            X_train, X_test, Y_train, Y_test = train_test_split(X, Y, test_size = 0.2)

            # Create a neural network: (dimensions will be based on heatmap work)
            model = Sequential()
            model.add(Dense(150, input_dim=num_inputs, kernel_initializer='normal', activation='relu'))
            for n in range(mlp_layers):
                model.add(Dense(neurons_per_layer, kernel_initializer='normal', activation='relu'))
            model.add(Dense(num_outputs, kernel_initializer='normal'))
            #loss_weights = [0.5] * num_outputs
            #loss_weights[-1] = 1.0
            #loss_weights[-2] = 1.0
            #loss_weights[-3] = 1.0
            #model.compile(loss=['mean_squared_error']*num_outputs, optimizer='adam',loss_weights=loss_weights, metrics=['mae']) #
            model.compile(loss='mean_squared_error', optimizer='adam', metrics=['mae']) #
            # train the model
            model.fit(X_train, Y_train, epochs=250, batch_size=35, verbose=1)

            # test
            start = time.time()
            scores = model.evaluate(X_test, Y_test)
            predictions = model.predict(X_test)
            end = time.time()

            rms_out = [0] * num_outputs
            for y, y_hat in zip(Y_test, predictions):
                for i, (val, val_hat) in enumerate(zip (y, y_hat)):
                    rms_out[i] += (val-val_hat)**2
            rms_out[:] = [x / len(Y_test) for x in rms_out]
            rms_error = [np.sqrt(x) for x in rms_out]

            print (str(len(Y_test)) + " evaluations in:")
            print(str(end - start) + " seconds")
            print("\nperformance on test set:" + str(scores[1]))
            print("%s: %.2f%%" % (model.metrics_names[1], scores[1]*100))

            # record the statistics to a file
            filename = sheet_label+"_stats.pickle"
            core_ave_DC_tot.append(rms_error[-3])
            core_ave_CTC_tot.append(rms_error[-2])
            bu_over_e.append(rms_error[-1])
            print("core_ave_DC_tot:  "+ str(np.mean(core_ave_DC_tot)) +"    core_ave_CTC_tot:  "+str(np.mean(core_ave_CTC_tot))+"    bu_over_e: "+ str(np.mean(bu_over_e)) )

            if os.path.isfile(filename):
                stats = pickle.load( open( filename, "rb" ) )
                stats.errors.append(scores[1])
                stats.per_category_error.append(rms_error)
                pickle.dump(stats, open( filename, "wb" ) )
            else:
                stats = record()
                stats.errors = [scores[1]]
                stats.per_category_error = [rms_error]
                pickle.dump(stats, open( filename, "wb" ) )
                print (stats.errors)
                print ("Running average over seeds is: " + str(np.mean(stats.errors))+"\n\n")

        except KeyError:
            print("did not find function in sheet dictionary")

if __name__ == "__main__":
    main()
