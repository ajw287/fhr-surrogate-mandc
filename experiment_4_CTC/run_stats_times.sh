#!/usr/bin/env bash
# Copyright 2018 Andrew Whyte & Zhiyao Xing
#
#Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
#The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#
#for i in  {123456000..123456030}
#do
#echo "python3 k_inf_model.py ./ALL_Kinf_results.xlsx singlePin_Triso $i"
#python3 k_inf_model.py ./ALL_Kinf_results.xlsx annularPin_UC $i
#python3 k_inf_model.py ./ALL_Kinf_results.xlsx annularPin_UC $i
#done
for filename in ./*.pickle; do
    python3 show_error_stats.py $filename
done
#singlePin_UC
#singlePin_Triso
#annularPin_UC
#annularPin_Triso
