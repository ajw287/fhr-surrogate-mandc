# Copyright 2018 Andrew Whyte & Zhiyao Xing
#
#Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
#The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#
import os
import sys
import pickle
import numpy as np

class record:
    pass

if len(sys.argv) >= 2:
    file = sys.argv[1]
    print ("loading file: "+file)
else:
    file = "output_stats.pickle"
    print ("loading default file: "+file)

if os.path.isfile(file):
    stats = pickle.load( open(file, "rb" ) )
    #print ("the record contains :"+str(len(stats.errors))+" error records")
    #print(stats.per_category_error)
    i = len(stats.per_category_error[0])-1
    j = len(stats.per_category_error[0])-2
    k = len(stats.per_category_error[0])-3
    eBU = [ebu[i] for ebu in stats.per_category_error]
    CTC = [ctc[j] for ctc in stats.per_category_error]
    DC = [dc[k] for dc in stats.per_category_error]
    print("eBU: "+str(np.mean(eBU)))
    print("CTC: "+str(np.mean(CTC)))
    print("DC: "+str(np.mean(DC)))
    #print ("the average value is:")
    #print(np.mean(stats.errors))
    #print ("the standard deviation is:")
    #print(np.std(stats.errors))
else:
    print("input file not found! ... exiting")
