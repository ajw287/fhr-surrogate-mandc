# Copyright 2018 Andrew Whyte & Zhiyao Xing
#
#Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
#The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#
import pandas as pd
import sys, os
import pickle
import numpy as np
import time

# have to set seeds before we import the keras
import random as rn
import tensorflow as tf

# make keras results repeatable, see:
# https://keras.io/getting-started/faq/#how-can-i-obtain-reproducible-results-using-keras-during-development
seed  = sys.argv[3]
# Max for PYTHONHASHSEED 4294967295
# Max for numpy 2**32
if seed == 'time':
    # shuffled time initialisation source:
    # https://github.com/schmouk/PyRandLib
    #t = int( time.time() * 1000.0 )
    #seed = str( ( ((t & 0x0f000000) >> 24) +
    #            ((t & 0x00ff0000) >>  8) +
    #            ((t & 0x0000ff00) <<  8) +
    #            ((t & 0x000000ff) << 24)   )  )
    seed = str(int( (time.time()*1000.0) % (2**32 - 1)))
#os.environ['PYTHONHASHSEED'] = '0'
np.random.seed(int(seed))
rn.seed(np.random.randint(0, 2**31 - 1, dtype='l'))
#
from keras import backend as K
tf.set_random_seed(np.random.randint(0, 2**31 - 1, dtype='l'))
session_conf = tf.ConfigProto(intra_op_parallelism_threads=1, inter_op_parallelism_threads=1)
sess = tf.Session(graph=tf.get_default_graph(), config=session_conf)
K.set_session(sess)

from keras.models import Sequential
from keras.layers import Dense, Activation
#from keras.models import load_model
#from keras.models import model_from_json
from keras.optimizers import Adam
from sklearn.preprocessing import MinMaxScaler
from sklearn.model_selection import train_test_split

#https://machinelearningmastery.com/reproducible-results-neural-networks-keras/
# set both the numpy random seed and the tensorflow random seed...


class record:
    pass

#mlp_layers = 9
#neurons_per_layer = 25

mlp_layers = 10
neurons_per_layer = 35

description = "\nA script to investigate \& model excel tabulated AGR/FHR design results\n\n"
example_usage = "usage : \n" + str(sys.argv[0])+ " excel_document sheet_label random_seed\n\n eg:\n"+str(sys.argv[0])+" ALL_Kinf_results.xlsx annularPin_UC 0\n\n"

def usage_error(string = ""):
    print ("\n\nUSAGE ERROR: \n"+string)
    print (str(sys.argv[0]) + description + example_usage)
    quit()

def preprocess (ins, outs):
    # do I need two instances of this?  I would like to recover the data afterwards?
    scaler_outputs = MinMaxScaler(feature_range=(0, 1))
    scaler_inputs = MinMaxScaler(feature_range=(0, 1))
    outs = scaler_outputs.fit_transform(outs)
    print("\n\ndebug test:")
    print (len(ins) )
    ins = scaler_inputs.fit_transform(ins)
    return np.array(ins), np.array(outs)

def annularPin(data_table):
    #drop the coolant column of data as it has been 1-k binarised in excel
    #data_table.drop('fuel', axis=1)
    #data_table.drop('coolant', axis=1)
    inputs = data_table.iloc[:, 4:10].values
    outputs = data_table.iloc[:, 10:11].values
    num_inputs = len(inputs[0]) # 6
    num_outputs = 1
    return inputs, outputs, num_inputs, num_outputs

def singlePin(data_table):
    #drop the coolant column of data as it has been 1-k binarised in excel
    #data_table.drop('fuel', axis=1)
    #data_table.drop('coolant', axis=1)
    inputs = data_table.iloc[:, 4:8].values
    outputs = data_table.iloc[:, 8:9].values
    num_inputs= len(inputs[0])
    num_outputs = 1
    return inputs, outputs, num_inputs, num_outputs

def plate(data_table):
    #drop the coolant column of data as it has been 1-k binarised in excel
    #data_table.drop('fuel', axis=1)
    #data_table.drop('coolant', axis=1)
    inputs = data_table.iloc[:, 4:9].values
    outputs = data_table.iloc[:, 9:10].values
    num_inputs= len(inputs[0])
    num_outputs = 1
    return inputs, outputs, num_inputs, num_outputs

# set the random seed
# uncomment the correct option
#sheet_label = "annUC"
#sheet_label = "solidPinUC_optimisationUse"
#sheet_label = "solidPinTriso"
sheet_list = ['annularPin_UC_NaFZrf', 'annularPin_UC_FLiBe',
			  'annularPin_Triso_NaFZrF', 'annularPin_Triso_FLiBe',
			  'singlePin_UC_FLiBe' ,'singlePin_UC_NaFZrF' , 'singlePin_UC_FLiNaK',
			  'singlePin_Triso_NaFZrF', 'singlePin_Triso_FLiBe',
			  'plate_Triso_FLiBe', 'plate_Triso_NaFZrF'
			  ]



def main():
    if 4 != len(sys.argv) or sys.argv[1] == "--help":
        usage_error()

    # import the specified sheet of the data
    excel_file = sys.argv[1]
    sheet_label = sys.argv[2]

    print ("Processing: "+str(sheet_label))
    if sheet_label not in sheet_list:
    	print ("Did not work - give a valid sheet name.  Options are: " + str(sheet_list))
    	exit()
    #try:
    #    os.path.exists(excel_file)
    #except:
    #    print("\n\nExcel file not found: " + excel_file + "\n")
    #    exit()
    agr_data = pd.read_excel(excel_file, sheet_name=sheet_label)#, skiprows=17)

    print(agr_data.head)
    print(agr_data.shape)
    sheet_dict = {}
    for sheet in sheet_list:
        if sheet.startswith('annular'):
            sheet_dict[sheet] = annularPin(agr_data)
        if sheet.startswith('single'):
            sheet_dict[sheet] = singlePin(agr_data)
        if sheet.startswith('plate'):
            sheet_dict[sheet] = plate(agr_data)
    print(sheet_dict)


    # python switch statements are horrible.
    try:
        inputs, outputs, num_inputs, num_outputs = sheet_dict[sheet_label]
    except KeyError:
        print("did not find function in sheet dictionary")

    print('\n\nprinting inputs\n\n')
    print(inputs)
    print('\n\nprinting outputs\n\n')
    print(outputs)

    #scales...
    X, Y = preprocess(inputs, outputs)

    print(X.shape)
    X_train, X_test, Y_train, Y_test = train_test_split(X, Y, test_size = 0.2)

    # Create a neural network: (dimensions will be based on heatmap work)
    model = Sequential()
    model.add(Dense(50, input_dim=num_inputs, kernel_initializer='normal', activation='relu'))
    for n in range(mlp_layers):
    	model.add(Dense(neurons_per_layer, kernel_initializer='normal', activation='relu'))
    model.add(Dense(num_outputs, kernel_initializer='normal'))
    model.compile(loss='mean_squared_error', optimizer='adam', metrics=['mae']) #
    # train the model
    model.fit(X_train, Y_train, epochs=250, batch_size=35, verbose=1)

    # test
    scores = model.evaluate(X_test, Y_test)
    print("\nperformance on test set:" + str(scores[1]))

    predictions = model.predict(X_test)
    print("%s: %.2f%%" % (model.metrics_names[1], scores[1]*100))

    # record the statistics to a file
    if os.path.isfile("output_stats.pickle"):
        stats = pickle.load( open( "output_stats.pickle", "rb" ) )
        stats.errors.append(scores[1])
        pickle.dump(stats, open( "output_stats.pickle", "wb" ) )
    else:
        stats = record()
        stats.errors = [scores[1]]
        pickle.dump(stats, open( "output_stats.pickle", "wb" ) )
        print (stats.errors)
        print ("Running average over seeds is: " + str(np.mean(stats.errors))+"\n\n")

if __name__ == "__main__":
    main()
