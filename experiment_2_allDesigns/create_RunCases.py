# Copyright 2018 Andrew Whyte & Zhiyao Xing
#
#Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
#The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#
import pandas as pd
import sys, os
import stat
import pickle
import numpy as np
import time
from shutil import copyfile

sheet_list = [ "annularPin_UC", "annularPin_Triso","plate_Triso", "singlePin_UC", "singlePin_Triso"]
sheet_list_Salt = ['annularPin_UC_NaFZrf', 'annularPin_UC_FLiBe',
			  'annularPin_Triso_NaFZrF', 'annularPin_Triso_FLiBe',
			  'singlePin_UC_FLiBe' ,'singlePin_UC_NaFZrF' , 'singlePin_UC_FLiNaK',
			  'singlePin_Triso_NaFZrF', 'singlePin_Triso_FLiBe',
			  'plate_Triso_FLiBe', 'plate_Triso_NaFZrF'
			  ]

if ((len(sys.argv)>1) and sys.argv[1] == 'clean'):
	for case in sheet_list_Salt:
		if os.path.exists(case):
			for file in os.listdir(case):
				os.remove(f'{case}/{file}')
			os.rmdir(case)
	for case in sheet_list:
		if os.path.exists(case):
			for file in os.listdir(case):
				os.remove(f'{case}/{file}')
			os.rmdir(case)
	exit()

for case in sheet_list_Salt:
	if not os.path.exists(case):
		os.makedirs(case)
		#copyfile('ALL_Kinf_results_BOC.xlsx', f'{case}/ALL_Kinf_results_BOC.xlsx')
		copyfile('k_inf_model_bySalt.py', f'{case}/k_inf_model_bySalt.py')
		copyfile('show_error_stats.py', f'{case}/show_error_stats.py')
		os.chdir(case)
		if (sys.platform == 'linux'):
			if not os.path.exists('run_30_times.sh'):
				with open ('run_30_times.sh' , 'w') as runfile:
					runfile.write('#!/usr/bin/env bash\n')
					runfile.write('for i in  {123456000..123456030}\n')
					runfile.write('do\n')
					runfile.write(f'    python3 k_inf_model_bySalt.py ../ALL_Kinf_results_BOC.xlsx {case} $i\n')
					runfile.write('done\n')
				st = os.stat('run_30_times.sh')
				os.chmod('run_30_times.sh', st.st_mode | stat.S_IEXEC)
			os.system('./run_30_times.sh')
		elif(sys.platform == 'win32'):
			if not os.path.exists('run_30_times.bat'):
				with open ('run_30_times.bat' , 'w') as runfile:
					runfile.write('echo off\n')
					runfile.write('title Run 30 training runs \n')
					runfile.write('for /l %%x in (123456000, 1, 123456030) do (\n')
					runfile.write('\techo Training run number %%x\n')
					runfile.write(f'\tpython3 k_inf_model_bySalt.py ../ALL_Kinf_results_BOC.xlsx {case} %%x\n')
					runfile.write(')\n')
			os.system('run_30_times.bat')
		else:
			print("unrecognised operating system")
			exit()
		os.chdir('..')

for case in sheet_list:
	if not os.path.exists(case):
		os.makedirs(case)
		#copyfile('ALL_Kinf_results_BOC.xlsx', f'{case}/ALL_Kinf_results_BOC.xlsx')
		copyfile('k_inf_model.py', f'{case}/k_inf_model.py')
		copyfile('show_error_stats.py', f'{case}/show_error_stats.py')
		os.chdir(case)
		if (sys.platform == 'linux'):
			if not os.path.exists('run_30_times.sh'):
				with open ('run_30_times.sh' , 'w') as runfile:
					runfile.write('#!/usr/bin/env bash\n')
					runfile.write('for i in  {123456000..123456030}\n')
					runfile.write('do\n')
					runfile.write(f'    python3 k_inf_model.py ../ALL_Kinf_results_BOC.xlsx {case} $i\n')
					runfile.write('done\n')
				st = os.stat('run_30_times.sh')
				os.chmod('run_30_times.sh', st.st_mode | stat.S_IEXEC)
			os.system('./run_30_times.sh')
		elif(sys.platform == 'win32'):
			if not os.path.exists('run_30_times.bat'):
				with open ('run_30_times.bat' , 'w') as runfile:
					runfile.write('echo off\n')
					runfile.write('title Run 30 training runs \n')
					runfile.write('for /l %%x in (123456000, 1, 123456030) do (\n')
					runfile.write('\techo Training run number %%x\n')
					runfile.write(f'\tpython k_inf_model.py ./ALL_Kinf_results_BOC.xlsx {case} %%x\n')
					runfile.write(')\n')
			os.system('run_30_times.bat')
		os.chdir('..')
