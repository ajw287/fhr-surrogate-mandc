import os

# Copyright 2018 Andrew Whyte & Zhiyao Xing
#
#Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
#The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#

sheet_list = [ "annularPin_UC", "annularPin_Triso","plate_Triso", "singlePin_UC", "singlePin_Triso"]
sheet_list_Salt = ['annularPin_UC_NaFZrf', 'annularPin_UC_FLiBe',
			  'annularPin_Triso_NaFZrF', 'annularPin_Triso_FLiBe',
			  'singlePin_UC_FLiBe' ,'singlePin_UC_NaFZrF' , 'singlePin_UC_FLiNaK',
			  'singlePin_Triso_NaFZrF', 'singlePin_Triso_FLiBe',
			  'plate_Triso_FLiBe', 'plate_Triso_NaFZrF'
			  ]

with open('ALL_results.txt','w') as output:
	output.write('ALL RESULTS IN THIS FOLDER ARE COLLECTED HERE')
	for case in sheet_list_Salt:
		os.chdir(case)
		os.system(f'python show_error_stats.py > log_{case}.txt')
		os.chdir('..')
		output.write(f'\n\nRESULTS FOR CASE: {case}\n\n')

		with open(f'{case}/log_{case}.txt','r') as file:
			lines = file.readlines()
			for line in lines:
				output.write(line)

	for case in sheet_list:
		os.chdir(case)
		os.system(f'python show_error_stats.py > log_{case}.txt')
		os.chdir('..')
		output.write(f'\n\nRESULTS FOR CASE: {case}\n\n')

		with open(f'{case}/log_{case}.txt','r') as file:
			lines = file.readlines()
			for line in lines:
				output.write(line)
